<?php
require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';


$sheep = new Animal("shaun");

echo "Nama binatang: ". $sheep->name. "<br>"; // "shaun"
echo "Jumlah kakinya: ". $sheep->legs."<br>"; // 2
echo "Berdarah dingin benar atau tidak: " .$sheep->cold_blooded."<br><br>";// false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


$sungokong = new Ape("kera sakti");
echo "Nama binatang: ". $sungokong->name. "<br>"; 
$sungokong->yell() ;// "Auooo"
echo "jumlah kakinya: ". $sungokong->legs."<br><br>"; 

$kodok = new Frog("buduk");
echo "Nama binatang: ". $kodok->name. "<br>"; 
$kodok->jump() ; // "hop hop"
echo "jumlah kakinya: ". $kodok->legs."<br>"; 
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


?>