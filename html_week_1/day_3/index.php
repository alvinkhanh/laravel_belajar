<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Hello, world!</title>
  </head>
  <body>
<div class="wrapper">
      <!-- topbar -->
<header>
<nav class="navbar navbar-expand-lg navbar-light bg-light main-topbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="image/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="topbar">
            <div class="collapse navbar-collapse topbar" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 justify-content-end">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">Gift Card</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Track Order</a>
                </li>
                
                <li class="nav-item">
                <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">Contact Us</a>
                </li>
            </ul>
        </div>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>
</header>
<!-- topbar close -->
<!--  -->
<div class="container-fluid" id="content">
    <!-- sup topbar -->
    <div class="sup-topbar">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-link" aria-current="page" href="#">HOME</a>
            <a class="nav-link" href="#">SHOP</a>
            <a class="nav-link" href="#">BLOG</a>
            <a class="nav-link" href="#" tabindex="-1" aria-disabled="true">ABOUT US</a>
        </nav>
    </div>
    <!-- sup topbar close -->

    <!-- side section -->
            <div class="row">
                <div class="col-lg-3">
                    <div class="item">
                        <div id="list-example" class="list-group">
                            <a class="list-group-item list-group-item-action" href="#list-item-1">Item 1</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-2">Item 2</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-3">Item 3</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-4">Item 4</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-1">Item 5</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-2">Item 6</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-3">Item 7</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-4">Item 8</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-1">Item 9</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-2">Item 10</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-3">Item 11</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-4">Item 12</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-2">Item 13</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-3">Item 14</a>
                            <a class="list-group-item list-group-item-action" href="#list-item-4">Item 15</a>
                        </div>
                    </div>
                </div>
            <!-- side section close -->
                 <!-- jumbotron -->
                <div class="col-lg-6">
                    <div class="tengah">
                    <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
  <div class="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
  </div>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="image/BG.png" class="d-block w-100" alt="image/bg2.jpg">
      <div class="carousel-caption d-none d-md-block">
        <h5>First slide label</h5>
        <p>Some representative placeholder content for the first slide.</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="image/bg2.jpg." class="d-block w-100" alt="...">
      <div class="carousel-caption d-none d-md-block">
        <h5>Second slide label</h5>
        <p>Some representative placeholder content for the second slide.</p>
      </div>
    </div>
  </div>
  <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"  data-bs-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

                        <!--untuk jombotronnya  -->
                        <!-- <div id="list-example" class="list-group">
                            <div class="jumbotron jumbotron-fluid">
                                <div class="container">
                                    <h1 class="display-4">Fluid jumbotron</h1>
                                    <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
                                </div>
                            </div>
                        </div> -->
                        <!-- close -->

                     <!-- card -->
                        <div class="row">
                            <div class="isi">
                                <div class="col-sm-4">
                                    <div class="card">
                                        <img src="image/naofumi.png" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card">
                                        <img src="image/rhaptalia.png" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card">
                                        <img src="image/firo.png" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <a href="#" class="btn btn-primary">Go somewhere</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- cardclose -->
                </div>
                <!-- jumbotron close -->

    <!-- feature -->
                    <div class="col-lg-3">
                        <div class="feature">
                            <div id="list-example" class="list-group">
                                <a class="list-group-item list-group-item-action" href="#list-item-1">Feature 1</a>
                                <a class="list-group-item list-group-item-action" href="#list-item-2">Feature 2</a>
                                <a class="list-group-item list-group-item-action" href="#list-item-3">Feature 3</a>
                                <a class="list-group-item list-group-item-action" href="#list-item-4">Feature 4</a>
                                <a class="list-group-item list-group-item-action" href="#list-item-5">Feature 5</a>
                                <a class="list-group-item list-group-item-action" href="#list-item-6">Feature 6</a>
                            </div>
                        </div>
                        <div class="external">
                            <div id="list-example" class="list-group">
                                <a class="list-group-item list-group-item-action" href="#external">External Links</a>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- feature close -->

</div>
<!-- content close -->
</div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
    -->
  </body>
</html>